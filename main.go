package main

import (
	"fmt"

	"github.com/tarm/serial"
)

func main() {
	fmt.Println("hello")
	c := &serial.Config{Name: "/dev/ttyS1", Baud: 38400, Parity: serial.ParityNone,
		StopBits: serial.Stop1, Size: serial.DefaultSize, ReadTimeout: 20}
	s, err := serial.OpenPort(c)
	if err != nil {
		fmt.Println(err)
	}
	n, err := s.Write([]byte("AT+ROLE=0\n"))
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(n)
	buf := make([]byte, 128)
	n, err = s.Read(buf)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(n)
	data := string(buf)
	fmt.Println(data)
	for {
		n, err = s.Read(buf)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(n)
		newData := string(buf)
		fmt.Println(newData)
	}
}
